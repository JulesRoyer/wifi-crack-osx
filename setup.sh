#! /bin/bash

# "Installation" de airport qui permet la découverte du réseau et la suite
printf "Veuillez entrer votre mot de passe pour que les opérations se dérouent comme prévu :\n";
sudo ln -s /System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport /usr/sbin/airport;

# Installation de homebrew si c'est pas déjà fait + dépendances et programmes utiles = hashcat tcpdump
sudo /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install hashcat tcpdump autoconf automake libtool openssl shtool pkg-config hwloc pcre sqlite3 libpcap cmocka

# Installation de JamWifi.app
curl http://macheads101.com/pages/downloads/mac/JamWiFi.app.zip
unzip JamWiFi.app.zip

# A suivre...